package org.feelife.controller.auth.logout;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.response.UserResponseToken;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.BearerTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Configuration
public class AuthorizationControllerLogoutTest {

    @Value("http://${server.address}:${server.port}/auth/logout")
    private String LogoutURL;

    @Value("http://${server.address}:${server.port}/auth/register")
    private String RegisterURL;

    private final ObjectMapper MAPPER = new ObjectMapper().findAndRegisterModules();

    private final AuthorizationRepository authorizationRepository;

    private final BearerTokenRepository bearerTokenRepository;

    @Autowired
    public AuthorizationControllerLogoutTest(AuthorizationRepository authorizationRepository, BearerTokenRepository bearerTokenRepository) {
        this.authorizationRepository = authorizationRepository;
        this.bearerTokenRepository = bearerTokenRepository;
    }

    @BeforeEach
    void deleteAll() {
        authorizationRepository.deleteAll();
        bearerTokenRepository.deleteAll();
    }

    private UserResponseToken createUserAccountAndReturnToken() {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "password", "i@email");
            HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                    .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setEntity(new StringEntity(userRegisterDTO.toString())).build();
            CloseableHttpResponse response = client.execute(httpPost);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new ParseException(EntityUtils.toString(response.getEntity()));
            }
            return MAPPER.readValue(EntityUtils.toString(response.getEntity()), UserResponseToken.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    void logoutWithCorrectToken() throws IOException {
        UserResponseToken userToken = createUserAccountAndReturnToken();
        HttpUriRequest request = new HttpGet(LogoutURL);
        assert userToken != null;
        String validateUserToken = "Bearer " + userToken.getToken();
        request.setHeader(HttpHeaders.AUTHORIZATION, validateUserToken);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    void logoutWithCorrectToken2Time_throwsNoContent() throws IOException {
        UserResponseToken userToken = createUserAccountAndReturnToken();
        HttpUriRequest request = new HttpGet(LogoutURL);
        assert userToken != null;
        String validateUserToken = "Bearer " + userToken.getToken();
        request.setHeader(HttpHeaders.AUTHORIZATION, validateUserToken);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
        response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    void logoutWithIncorrectToken_throwsNoContent() throws IOException {
        UserResponseToken userToken = createUserAccountAndReturnToken();
        HttpUriRequest request = new HttpGet(LogoutURL);
        assert userToken != null;
        String validateUserToken = "Bearer " + userToken.getToken() + "sdfsEefj";
        request.setHeader(HttpHeaders.AUTHORIZATION, validateUserToken);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    void logoutWithIncorrectHeader_throwsBadRequest() throws IOException {
        UserResponseToken userToken = createUserAccountAndReturnToken();
        HttpUriRequest request = new HttpGet(LogoutURL);
        assert userToken != null;
        String validateUserToken = "Bearer " + userToken.getToken();
        request.setHeader(HttpHeaders.EXPIRES, validateUserToken);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void logoutWithoutHeader_throwsBadRequest() throws IOException {
        HttpUriRequest request = new HttpGet(LogoutURL);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }
}
