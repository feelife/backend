package org.feelife.controller.auth.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.feelife.model.dto.UserLoginDto;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.response.UserResponseToken;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.BearerTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Configuration
public class AuthorizationControllerLoginTest {

    @Value("http://${server.address}:${server.port}/auth/login")
    private String LoginURL;

    @Value("http://${server.address}:${server.port}/auth/register")
    private String RegisterURL;

    private final ObjectMapper MAPPER = new ObjectMapper().findAndRegisterModules();

    private final AuthorizationRepository authorizationRepository;

    private final BearerTokenRepository bearerTokenRepository;

    @Autowired
    public AuthorizationControllerLoginTest(AuthorizationRepository authorizationRepository, BearerTokenRepository bearerTokenRepository) {
        this.authorizationRepository = authorizationRepository;
        this.bearerTokenRepository = bearerTokenRepository;
    }

    @BeforeEach
    void deleteAll() {
        authorizationRepository.deleteAll();
        bearerTokenRepository.deleteAll();
    }

    private void createUserAccount() {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "password", "i@email");
            HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                    .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setEntity(new StringEntity(userRegisterDTO.toString())).build();
            CloseableHttpResponse response = client.execute(httpPost);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new ParseException(EntityUtils.toString(response.getEntity()));
            }
            System.out.println("CREATED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void loginUserThatExistWithCorrectData() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("i@email", "password");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new ParseException(EntityUtils.toString(response.getEntity()));
        }
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
        String jsonResponse = EntityUtils.toString(response.getEntity());
        UserResponseToken token = MAPPER.readValue(jsonResponse, UserResponseToken.class);
        assertThat(token).isNotNull();
    }

    @Test
    void loginUserThatExistWithInvalidEmail_throwsBadRequest() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("i@ema il", "password");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void loginUserThatExistWithInvalidPassword_throwsBadRequest() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("i@email", "pa ssw-or-d1");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void loginUserThatExistButWriteWrongPassword_throwsBadRequest() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("i@email", "wrongpassword");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void loginUserWithEmptyEmailField_throwsBadRequest() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("", "wrongpassword");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void loginUserWithEmptyPasswordField_throwsBadRequest() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("i@email", "");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void loginUserThatDoesNotExist_throwsNotFound() throws IOException {
        createUserAccount();
        CloseableHttpClient client = HttpClients.createDefault();
        UserLoginDto userLoginDTO = new UserLoginDto("new@email", "Newpassword");
        HttpUriRequest httpPost = RequestBuilder.post(LoginURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userLoginDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_NOT_FOUND);
    }
}
