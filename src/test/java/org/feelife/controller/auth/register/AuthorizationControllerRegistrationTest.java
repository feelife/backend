package org.feelife.controller.auth.register;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.response.UserResponseToken;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.BearerTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Configuration
class AuthorizationControllerRegistrationTest {

    private final ObjectMapper MAPPER = new ObjectMapper().findAndRegisterModules();

    private final AuthorizationRepository authorizationRepository;

    private final BearerTokenRepository bearerTokenRepository;

    @Value("http://${server.address}:${server.port}/auth/register")
    private String RegisterURL;

    @Autowired
    public AuthorizationControllerRegistrationTest(AuthorizationRepository authorizationRepository, BearerTokenRepository bearerTokenRepository) {
        this.authorizationRepository = authorizationRepository;
        this.bearerTokenRepository = bearerTokenRepository;
    }

    @BeforeEach
    void deleteAll() {
        authorizationRepository.deleteAll();
        bearerTokenRepository.deleteAll();
    }

    @Test
    void registerNewUserWithValidData() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "passwordd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new ParseException(EntityUtils.toString(response.getEntity()));
        }
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
        String jsonResponse = EntityUtils.toString(response.getEntity());
        UserResponseToken token = MAPPER.readValue(jsonResponse, UserResponseToken.class);
        assertThat(token).isNotNull();

    }

    @Test
    void registerNewUserWithValidDataSame() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "passwordd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
        String jsonResponse = EntityUtils.toString(response.getEntity());
        UserResponseToken token = MAPPER.readValue(jsonResponse, UserResponseToken.class);
        assertThat(token).isNotNull();
    }

    @Test
    void registerUserWithAlreadyUsedUsername_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "password", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);

        UserRegisterDTO userRegisterDTO2 = new UserRegisterDTO("inforest", "password", "i@email");
        httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO2.toString())).build();
        response = client.execute(httpPost);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerNewUserWithEmptyUsername_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("", "passwordd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerNewUserWithEmptyEmail_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "passwordd", "");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerNewUserWithEmptyPassword_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerNewUserWithEmptyFields_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("", "", "");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerUserWithTooShortUsername_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("it", "passwordd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);

    }

    @Test
    void registerUserWithTooShortPassword_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("infor", "pordd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerUserWithInvalidEmail_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("infdsor", "porddfsdfsd", "i@em s ail");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerUserWithInvalidSymbolsInUsername_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("info r ыва", "posdfsrdd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void registerUserWithInvalidSymbolsInPassword_throwsBadRequest() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO("infor", "posdf rdd", "i@email");
        HttpUriRequest httpPost = RequestBuilder.post(RegisterURL)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setEntity(new StringEntity(userRegisterDTO.toString())).build();
        CloseableHttpResponse response = client.execute(httpPost);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }
}
