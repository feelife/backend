package org.feelife.controller.activity;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.feelife.model.dto.ActivityIdDto;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.entity.ActivityEntity;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.BearerTokenRepository;
import org.feelife.repository.activity.ActivityRepository;
import org.feelife.repository.activity.UsersActivityRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Configuration
public class ActivityTests {

    @Value("http://${server.address}:${server.port}/activity")
    private String activityURL;

    @Value("http://${server.address}:${server.port}/auth/register")
    private String registerURL;

    private final AuthorizationRepository authorizationRepository;

    private final BearerTokenRepository bearerTokenRepository;

    private final ActivityRepository activityRepository;

    private final UsersActivityRepository usersActivityRepository;

    @Autowired
    public ActivityTests(AuthorizationRepository authorizationRepository, BearerTokenRepository bearerTokenRepository, ActivityRepository activityRepository, UsersActivityRepository usersActivityRepository) {
        this.authorizationRepository = authorizationRepository;
        this.bearerTokenRepository = bearerTokenRepository;
        this.activityRepository = activityRepository;
        this.usersActivityRepository = usersActivityRepository;
    }

    private String getUserToken() {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            UserRegisterDTO userRegisterDTO = new UserRegisterDTO("inforest", "password", "i@email");
            HttpUriRequest httpPost = RequestBuilder.post(registerURL)
                    .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setEntity(new StringEntity(userRegisterDTO.toString())).build();
            CloseableHttpResponse response = client.execute(httpPost);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new ParseException(EntityUtils.toString(response.getEntity()));
            }
            JSONObject object = new JSONObject(EntityUtils.toString(response.getEntity()));
            return object.getString("token");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private CloseableHttpResponse newActivityGetResponse(final String token) throws IOException {
        HttpUriRequest request = new HttpGet(activityURL + "/new");
        request.setHeader(HttpHeaders.AUTHORIZATION, token);
        return HttpClientBuilder.create().build().execute(request);
    }

    private CloseableHttpResponse activityByIDGetResponse(final String token, final Integer id) throws IOException {
        HttpUriRequest request = new HttpGet(activityURL + String.format("/%s", id));
        request.setHeader(HttpHeaders.AUTHORIZATION, token);
        return HttpClientBuilder.create().build().execute(request);
    }

    private CloseableHttpResponse takeActivityPostResponse(final String token, final Integer activityId) throws IOException, JSONException {
        JSONObject json = new JSONObject();
        json.put("activity_id", activityId);
        HttpUriRequest request = RequestBuilder.post(activityURL + "/take")
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setHeader(HttpHeaders.AUTHORIZATION, token)
                .setEntity(new StringEntity(json.toString())).build();
        return HttpClientBuilder.create().build().execute(request);
    }

    @BeforeEach
    void prepareDB() {
        activityRepository.save(new ActivityEntity("active1", "description", 60));
    }

    @AfterEach
    void deleteAll() {
        authorizationRepository.deleteAll();
        bearerTokenRepository.deleteAll();
        activityRepository.deleteAll();
        usersActivityRepository.deleteAll();
    }

    @Test
    void getActivityWithAcceptedAuthHeader() throws IOException {
        String token = "Bearer " + getUserToken();
        CloseableHttpResponse response = newActivityGetResponse(token);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    void getActivityWithInvalidAuthHeader_throwsNoContent() throws IOException {
        String token = "Bearer " + getUserToken() + "noize";
        CloseableHttpResponse response = newActivityGetResponse(token);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    void getActivityWithInvalidAuthHeader_throwsBadRequest() throws IOException {
        String token = "Ber " + getUserToken();
        CloseableHttpResponse response = newActivityGetResponse(token);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void getActivityWithoutAuthHeader() throws IOException {
        String token = null;
        CloseableHttpResponse response = newActivityGetResponse(token);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void getActivityByIdThatDoesNotExist() throws IOException {
        String token = "Bearer " + getUserToken();
        CloseableHttpResponse response = activityByIDGetResponse(token, 8);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void takeActivityThatDoesNotExist() throws IOException, JSONException {
        String token = "Bearer " + getUserToken();
        CloseableHttpResponse response = takeActivityPostResponse(token, 11);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    void takeActivityInvalidJson() throws IOException {
        String token = "Bearer " + getUserToken();
        HttpUriRequest request = RequestBuilder.post(activityURL + "/take")
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setHeader(HttpHeaders.AUTHORIZATION, token)
                .setEntity(new StringEntity(new ActivityIdDto(6).toString())).build();
        CloseableHttpResponse response =  HttpClientBuilder.create().build().execute(request);
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }
}
