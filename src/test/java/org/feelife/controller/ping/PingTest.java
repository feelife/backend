package org.feelife.controller.ping;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Configuration
public class PingTest {

    @Value("http://${server.address}:${server.port}/ping")
    private String pingURL;

    private CloseableHttpResponse getResponse() throws IOException {
        return HttpClientBuilder.create().build().execute(new HttpGet(pingURL));
    }

    @Test
    void checkPing() throws IOException {
        CloseableHttpResponse response = getResponse();
        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }
}
