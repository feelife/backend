package org.feelife.model.response;

public class UserWithBearerTokenIdResponse {

    private final Integer userId;

    private final Integer bearerTokenId;

    private final String userPassword;

    private final String userUsername;

    private final String userEmail;

    public UserWithBearerTokenIdResponse(
            Integer userId,
            Integer bearerTokenId,
            String userPassword,
            String userUsername,
            String userEmail) {
        this.userId = userId;
        this.bearerTokenId = bearerTokenId;
        this.userPassword = userPassword;
        this.userUsername = userUsername;
        this.userEmail = userEmail;
    }

    public Integer getBearerTokenId() {
        return bearerTokenId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public Integer getUserId() {
        return userId;
    }
}
