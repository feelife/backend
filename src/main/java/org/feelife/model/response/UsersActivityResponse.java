package org.feelife.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsersActivityResponse {

    private final Integer activityId;

    private final String name;

    private final String description;

    private final Integer estimatedTime;

    private final Boolean isTaken;

    private final Boolean isCompleted;

    public UsersActivityResponse(Integer activityId, String name, String description, Integer estimatedTime, Boolean isTaken, Boolean isCompleted) {
        this.activityId = activityId;
        this.name = name;
        this.description = description;
        this.estimatedTime = estimatedTime;
        this.isTaken = isTaken;
        this.isCompleted = isCompleted;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @JsonProperty(value = "estimated_time")
    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    @JsonProperty(value = "is_taken")
    public Boolean getTaken() {
        return isTaken;
    }

    @JsonProperty(value = "is_completed")
    public Boolean getCompleted() {
        return isCompleted;
    }

    @JsonProperty(value = "activity_id")
    public Integer getActivityId() {
        return activityId;
    }
}
