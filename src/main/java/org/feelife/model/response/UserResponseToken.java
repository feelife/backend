package org.feelife.model.response;

public class UserResponseToken {

    private final String token;

    private final String expires;

    public UserResponseToken(final String token, final String expiresTime) {
        this.token = token;
        this.expires = expiresTime;
    }

    public String getToken() {
        return token;
    }

    public String getExpires() { return expires; }
}
