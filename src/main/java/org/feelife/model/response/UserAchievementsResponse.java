package org.feelife.model.response;


public class UserAchievementsResponse {

    private final String name;

    public UserAchievementsResponse(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
