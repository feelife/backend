package org.feelife.model.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;

public class SymbolsValidator implements ConstraintValidator<ApprovedSymbols, String> {

   private final HashSet<Character> approved = initHashSet();

   @Override
   public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
      for (char c : password.toCharArray()) {
         if (!approved.contains(c)) {
            return false;
         }
      }
      return true;
   }

   private HashSet<Character> initHashSet() {
      HashSet<Character> returnedSet = new HashSet<>();
      String approvedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
      for (char c : approvedSymbols.toCharArray()) {
         returnedSet.add(c);
      }
      return returnedSet;
   }
}
