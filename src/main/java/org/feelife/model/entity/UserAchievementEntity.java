package org.feelife.model.entity;

import org.feelife.model.entity.keys.UserAchievementIds;

import javax.persistence.*;


@Entity
@Table(name = "user_achievement")
@IdClass(UserAchievementIds.class)
public class UserAchievementEntity {

    @Id
    @Column(name = "achievement_id")
    private Integer achievementId;

    @Id
    @Column(name = "user_id")
    private Integer userId;

    public UserAchievementEntity() {}

    public UserAchievementEntity(final Integer achievementId, final Integer userId) {
        this.achievementId = achievementId;
        this.userId = userId;
    }

    public Integer getAchievementId() {
        return achievementId;
    }

    public Integer getUserId() {
        return userId;
    }
}
