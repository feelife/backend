package org.feelife.model.entity;

import org.feelife.model.entity.keys.UsersActivityIds;

import javax.persistence.*;

@Entity
@Table(name = "users_activity")
@IdClass(UsersActivityIds.class)
public class UsersActivityEntity {

    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Id
    @Column(name = "activity_id")
    private Integer activityId;

    @Column(name = "is_completed", columnDefinition = "boolean default false", nullable = false)
    private Boolean isCompleted;

    @Column(name = "is_taken", columnDefinition = "boolean default false", nullable = false)
    private Boolean isTaken;

    public UsersActivityEntity() {}

    public UsersActivityEntity(Integer userId, Integer activityId) {
        this.userId = userId;
        this.activityId = activityId;
        this.isCompleted = false;
        this.isTaken = false;
    }

    public UsersActivityEntity(Integer userId, Integer activityId, Boolean isTaken) {
        this.userId = userId;
        this.activityId = activityId;
        this.isTaken = isTaken;
        this.isCompleted = false;
    }

    public UsersActivityEntity(Integer userId, Integer activityId, Boolean isTaken, Boolean isCompleted) {
        this.userId = userId;
        this.activityId = activityId;
        this.isTaken = isTaken;
        this.isCompleted = isCompleted;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }

    public Boolean getTaken() {
        return isTaken;
    }

    public void setTaken(Boolean taken) {
        isTaken = taken;
    }
}
