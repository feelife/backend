package org.feelife.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "bearer_token")
public class BearerTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Column(name = "token", nullable = false, unique = true)
    private String token;

    public BearerTokenEntity() {
    }

    public BearerTokenEntity(final Integer userId, final String token) {
        this.userId = userId;
        this.token = token;
    }

    public BearerTokenEntity(final Integer id, final Integer userId, final String token) {
        this.id = id;
        this.userId = userId;
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
