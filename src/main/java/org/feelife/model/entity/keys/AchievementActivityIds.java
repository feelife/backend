package org.feelife.model.entity.keys;

import java.io.Serializable;


public class AchievementActivityIds implements Serializable {

    private Integer achievementId;

    private Integer activityId;

    public AchievementActivityIds() {}

    public AchievementActivityIds(final Integer achievementId, final Integer activityId) {
        this.achievementId = achievementId;
        this.activityId = activityId;
    }
}
