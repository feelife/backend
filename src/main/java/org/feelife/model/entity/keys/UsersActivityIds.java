package org.feelife.model.entity.keys;

import java.io.Serializable;

public class UsersActivityIds implements Serializable {

    private Integer userId;

    private Integer activityId;

    public UsersActivityIds() {}

    public UsersActivityIds(Integer userId, Integer activityId) {
        this.userId = userId;
        this.activityId = activityId;
    }
}
