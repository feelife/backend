package org.feelife.model.entity.keys;

import java.io.Serializable;


public class UserAchievementIds implements Serializable {

    private Integer achievementId;

    private Integer userId;

    public UserAchievementIds() {}

    public UserAchievementIds(final Integer achievementId, final Integer userId) {
        this.achievementId = achievementId;
        this.userId = userId;
    }
}
