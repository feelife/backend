package org.feelife.model.entity;

import org.feelife.model.entity.keys.AchievementActivityIds;

import javax.persistence.*;


@Entity
@Table(name = "achievement_activity")
@IdClass(AchievementActivityIds.class)
public class AchievementActivityEntity {

    @Id
    @Column(name = "achievement_id")
    private Integer achievementId;

    @Id
    @Column(name = "activity_id")
    private Integer activityId;

    public AchievementActivityEntity() {}

    public AchievementActivityEntity(final Integer achievementId, final Integer activityId) {
        this.achievementId = achievementId;
        this.activityId = activityId;
    }

    public Integer getAchievementId() {
        return achievementId;
    }

    public Integer getActivityId() {
        return activityId;
    }
}
