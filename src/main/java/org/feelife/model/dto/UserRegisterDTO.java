package org.feelife.model.dto;

import com.sun.istack.NotNull;
import org.feelife.model.annotation.ApprovedSymbols;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class UserRegisterDTO {

    @NotNull
    @NotBlank(message = "Field username can not be empty")
    @Size(min = 4, max = 32, message = "Length of username must be more than 3 symbols and less than 33 symbols.")
    @ApprovedSymbols(message = "All approved symbols in username is: English letters, digits and _")
    private final String username;

    @NotNull
    @NotBlank(message = "Field password can not be empty")
    @Size(min = 8, message = "Length of password must be at least 8 symbols.")
    @ApprovedSymbols(message = "All approved symbols in password is: English letters, digits and _")
    private final String password;

    @NotNull
    @Email(message = "Email should be valid")
    @NotBlank(message = "Field email can not be empty")
    private final String email;

    public UserRegisterDTO(final String username, final String password, final String email) {
        this.username = username.strip();
        this.password = password.strip();
        this.email = email.strip();
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "{" +
                "\"username\":\"" + username + '\"' +
                ", \"password\":\"" + password + '\"' +
                ", \"email\":\"" + email + '\"' +
                '}';
    }
}
