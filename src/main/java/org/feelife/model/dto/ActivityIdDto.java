package org.feelife.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import javax.validation.constraints.NotNull;

public class ActivityIdDto {

    @JsonAlias({"activity_id"})
    @NotNull
    private Integer activityId;

    public ActivityIdDto() {}

    public ActivityIdDto(final Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getActivityId() {
        return activityId;
    }
}
