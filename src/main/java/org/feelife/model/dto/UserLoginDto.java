package org.feelife.model.dto;

import com.sun.istack.NotNull;
import org.feelife.model.annotation.ApprovedSymbols;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserLoginDto {

    @NotNull
    @Email(message = "Email should be valid")
    @NotBlank(message = "Field email can not be empty")
    private final String email;

    @NotNull
    @NotBlank(message = "Field password can not be empty")
    @Size(min = 8, message = "Length of password must be at least 8 symbols.")
    @ApprovedSymbols(message = "All approved symbols in password is: English letters, digits and _")
    private final String password;

    public UserLoginDto(final String email, final String password) {
        this.email = email.strip();
        this.password = password.strip();
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "{" +
                "\"email\":\"" + email + '\"' +
                ", \"password\":\"" + password + '\"' +
                '}';
    }
}
