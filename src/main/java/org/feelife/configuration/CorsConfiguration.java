package org.feelife.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CorsConfiguration implements WebMvcConfigurer {

    @Value("${feelife.debug}")
    private Boolean debug;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(final CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("https://feelife-iu.web.app/");
                if (debug) {
                    registry.addMapping("/**").allowedOrigins(
                            "http://localhost:3000",
                            "http://localhost:3000/",
                            "https://feelife-iu.web.app/");
                }
            }
        };
    }
}
