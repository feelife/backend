package org.feelife.service;


import io.jsonwebtoken.ExpiredJwtException;
import org.feelife.exception.entity.*;
import org.feelife.model.dto.UserLoginDto;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.entity.BearerTokenEntity;
import org.feelife.model.entity.UserEntity;
import org.feelife.model.response.UserResponseToken;
import org.feelife.model.response.UserWithBearerTokenIdResponse;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.BearerTokenRepository;
import org.feelife.util.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;


@Service
public class AuthorizationService {

    private final AuthorizationRepository authorizationRepository;

    private final BearerTokenRepository bearerTokenRepository;

    private final Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

    @Autowired
    public AuthorizationService(AuthorizationRepository authorizationRepository, BearerTokenRepository bearerTokenRepository) {
        this.authorizationRepository = authorizationRepository;
        this.bearerTokenRepository = bearerTokenRepository;
    }

    /***
     * Add new user if it username not from DB
     * @param userRegisterDTO store username, email and password of user
     * @return user bearer token
     */
    public UserResponseToken addUser(final UserRegisterDTO userRegisterDTO) {
        if (authorizationRepository.findUserByUsername(userRegisterDTO.getUsername()).isPresent()) {
            throw new UserWithThatUsernameIsAlreadyExistException(String.format("User with username: %s is already exist", userRegisterDTO.getUsername()));
        }
        if (authorizationRepository.findUserByEmail(userRegisterDTO.getEmail()).isPresent()) {
            throw new UserWithThatEmailIsAlreadyExistException(String.format("User with email: %s is already exist", userRegisterDTO.getEmail()));
        }
        UserEntity user = new UserEntity(
                userRegisterDTO.getUsername(),
                userRegisterDTO.getEmail(),
                SecurityUtil.encodePassword(userRegisterDTO.getPassword())
        );
        authorizationRepository.save(user);
        String jwsToken = SecurityUtil.getNewJwtToken(user.getId().toString());
        bearerTokenRepository.save(new BearerTokenEntity(user.getId(), jwsToken));
        return new UserResponseToken(jwsToken, DateTimeFormatter.ISO_DATE_TIME.format(OffsetDateTime.now().plusSeconds(SecurityUtil.SEC_IN_ONE_MONTH)));
    }

    /***
     * Check user from DB and correct data. If data is correct then save new user's bearer token
     * @param userLoginDTO store email and password of login user
     * @return new user bearer token
     */
    public UserResponseToken loginUser(final UserLoginDto userLoginDTO) {

        final UserEntity user = authorizationRepository.findUserByEmail(userLoginDTO.getEmail())
                .orElseThrow(() -> new UserWithThatEmailDoesNotExistException(String.format("User with email: %s does not exist", userLoginDTO.getEmail())));

        final Optional<UserWithBearerTokenIdResponse> data = bearerTokenRepository.getIdByUserEmail(userLoginDTO.getEmail());

        if (!SecurityUtil.validateResponsePassword(userLoginDTO.getPassword(), user.getPassword())) {
            logger.info(String.format("User with id: %s write wrong password", user.getId()));
            throw new WrongPasswordException("Wrong password");
        }
        String jwsToken = SecurityUtil.getNewJwtToken(user.getId().toString());

        if (data.isPresent()) {
            bearerTokenRepository.save(new BearerTokenEntity(data.get().getBearerTokenId(), data.get().getUserId(), jwsToken));
        } else {
            bearerTokenRepository.save(new BearerTokenEntity(user.getId(), jwsToken));
        }
        return new UserResponseToken(jwsToken, DateTimeFormatter.ISO_DATE_TIME.format(OffsetDateTime.now().plusSeconds(SecurityUtil.SEC_IN_ONE_MONTH)));
    }

    /***
     * Delete all user's token when this user Log Out
     * @param userTokenRaw receive user's token from header Authorization
     * return Status OK if this token is valid
     */
    public void deleteUserToken(final String userTokenRaw) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        try {
            SecurityUtil.isTokenExpired(userToken);
        } catch (ExpiredJwtException exception) {
            logger.info(String.format("Token expired for user with id: %s", userId));
        }
        bearerTokenRepository.deleteBearerTokenEntityByUserId(userId);
    }
}
