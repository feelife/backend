package org.feelife.service;

import io.jsonwebtoken.JwtException;
import org.feelife.repository.BearerTokenRepository;
import org.feelife.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class TokenCheckerService {

    private final BearerTokenRepository bearerTokenRepository;

    @Autowired
    public TokenCheckerService(final BearerTokenRepository bearerTokenRepository) {
        this.bearerTokenRepository = bearerTokenRepository;
    }

    public Boolean isTokenValid(final String token) {
        if (SecurityUtil.isJwtTokenInvalid(token)) {
            return false;
        }
        final String userToken = token.substring(token.indexOf(' ') + 1);
        try {
            SecurityUtil.isTokenExpired(userToken);
        } catch (JwtException e) {
            return false;
        }
        final Optional<Integer> id = bearerTokenRepository.getUserIdByToken(userToken);
        return id.isPresent();
    }
}
