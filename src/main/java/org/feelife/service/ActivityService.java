package org.feelife.service;

import org.feelife.exception.entity.ActivityNotFoundException;
import org.feelife.exception.entity.TokenNotFoundException;
import org.feelife.model.dto.ActivityIdDto;
import org.feelife.model.entity.ActivityEntity;
import org.feelife.model.entity.UserAchievementEntity;
import org.feelife.model.entity.UsersActivityEntity;
import org.feelife.model.entity.keys.UsersActivityIds;
import org.feelife.model.response.UsersActivityResponse;
import org.feelife.repository.BearerTokenRepository;
import org.feelife.repository.achievement.UserAchievementRepository;
import org.feelife.repository.activity.ActivityRepository;
import org.feelife.repository.activity.UsersActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    private final UsersActivityRepository usersActivityRepository;

    private final BearerTokenRepository bearerTokenRepository;

    private final UserAchievementRepository userAchievementRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository,
                           UsersActivityRepository usersActivityRepository,
                           BearerTokenRepository bearerTokenRepository,
                           final UserAchievementRepository userAchievementRepository) {
        this.activityRepository = activityRepository;
        this.usersActivityRepository = usersActivityRepository;
        this.bearerTokenRepository = bearerTokenRepository;
        this.userAchievementRepository = userAchievementRepository;
    }

    public UsersActivityResponse getActivity(final String userTokenRaw) {
        final String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        final Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        final Optional<List<UsersActivityResponse>> usersActivities = activityRepository.getAllUsersActivities(userId);
        if (usersActivities.get().size() > 0) {
            return usersActivities.get().get(new Random().nextInt(usersActivities.get().size()));
        }
        final Optional<List<Integer>> acceptedUserActivities = activityRepository.getAllAcceptedActivities(userId);
        if (acceptedUserActivities.get().size() == 0) {
            throw new ActivityNotFoundException("Seems you did all challenges at this moment!");
        }
        final Integer activityId = acceptedUserActivities.get().get(new Random().nextInt(acceptedUserActivities.get().size()));
        usersActivityRepository.save(new UsersActivityEntity(userId, activityId));
        final ActivityEntity activityEntity = activityRepository.findById(activityId)
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
        return new UsersActivityResponse(
                activityId,
                activityEntity.getName(),
                activityEntity.getDescription(),
                activityEntity.getEstimatedTime(),
                false,
                false);
    }

    public ActivityEntity getActivityById(final String userTokenRaw, final Integer activityId) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        return activityRepository.getUsersActivityByActivityID(activityId, userId)
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
    }

    public List<UsersActivityResponse> getAll(final String userTokenRaw) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        return activityRepository.getAllUsersActivities(userId)
                .orElseThrow(() -> new ActivityNotFoundException("User's activities not found"));
    }

    public UsersActivityResponse takeActivity(final String userTokenRaw, final ActivityIdDto activityIdDTO) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        Integer activityId = activityIdDTO.getActivityId();
        final ActivityEntity activityEntity = activityRepository.findById(activityId)
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
        usersActivityRepository.findById(new UsersActivityIds(userId, activityId))
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
        usersActivityRepository.save(new UsersActivityEntity(userId, activityId, true));
        return new UsersActivityResponse(
                activityId,
                activityEntity.getName(),
                activityEntity.getDescription(),
                activityEntity.getEstimatedTime(),
                true,
                false
        );
    }

    public UsersActivityResponse completeActivity(final String userTokenRaw, final ActivityIdDto activityIdDTO) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        Integer activityId = activityIdDTO.getActivityId();
        usersActivityRepository.findById(new UsersActivityIds(userId, activityId))
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
        usersActivityRepository.save(new UsersActivityEntity(userId, activityId, true, true));
        final ActivityEntity activityEntity = activityRepository.findById(activityId)
                .orElseThrow(() -> new ActivityNotFoundException("Activity not found"));
        achievementUpdate(userId);
        return new UsersActivityResponse(
                activityId,
                activityEntity.getName(),
                activityEntity.getDescription(),
                activityEntity.getEstimatedTime(),
                true,
                true
        );
    }

    private void achievementUpdate(final Integer userId) {
        Optional<HashSet<Integer>> userCompletedActivities = usersActivityRepository.findAllCompletedActivitiesByUserId(userId);
        if (userCompletedActivities.isPresent()) {
            final Integer achievementAmount = userCompletedActivities.get().size();
            switch (achievementAmount) {
                case (1):
                    userAchievementRepository.save(new UserAchievementEntity(1, userId));
                    break;
                case (5):
                    userAchievementRepository.save(new UserAchievementEntity(2, userId));
                    break;
                case (10):
                    userAchievementRepository.save(new UserAchievementEntity(3, userId));
                    break;
                case (15):
                    userAchievementRepository.save(new UserAchievementEntity(4, userId));
                    break;
            }
        }
    }
}
