package org.feelife.service;

import org.feelife.exception.entity.TokenNotFoundException;
import org.feelife.model.response.UserAchievementsResponse;
import org.feelife.repository.BearerTokenRepository;
import org.feelife.repository.achievement.AchievementActivityRepository;
import org.feelife.repository.achievement.AchievementRepository;
import org.feelife.repository.achievement.UserAchievementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class AchievementService {

    private final BearerTokenRepository bearerTokenRepository;

    private final AchievementRepository achievementRepository;

    private final AchievementActivityRepository achievementActivityRepository;

    private final UserAchievementRepository userAchievementRepository;

    @Autowired
    public AchievementService(
            final BearerTokenRepository bearerTokenRepository,
            final AchievementRepository achievementRepository,
            final AchievementActivityRepository achievementActivityRepository,
            final UserAchievementRepository userAchievementRepository) {
        this.bearerTokenRepository = bearerTokenRepository;
        this.achievementRepository = achievementRepository;
        this.achievementActivityRepository = achievementActivityRepository;
        this.userAchievementRepository = userAchievementRepository;
    }

    public List<UserAchievementsResponse> getUsersAchievement(final String userTokenRaw) {
        String userToken = userTokenRaw.substring(userTokenRaw.indexOf(' ') + 1);
        Integer userId = bearerTokenRepository.getUserIdByToken(userToken)
                .orElseThrow(() -> new TokenNotFoundException("Invalid token"));
        Optional<List<UserAchievementsResponse>> usersAchievements = userAchievementRepository.getUserAchievements(userId);
        return usersAchievements.get();
    }


}
