package org.feelife.exception.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseException {

    private final String message;

    private final String errorType;

    public ResponseException(final String message, final String errorType) {
        this.message = message;
        this.errorType = errorType;
    }

    @JsonProperty("error_type")
    public String getErrorType() { return errorType; }

    public String getMessage() {
        return message;
    }
}
