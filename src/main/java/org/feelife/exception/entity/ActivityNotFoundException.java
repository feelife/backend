package org.feelife.exception.entity;

public class ActivityNotFoundException extends RuntimeException {
    public ActivityNotFoundException(final String message) {
        super(message);
    }
}
