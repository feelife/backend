package org.feelife.exception.entity;

public class UserWithThatUsernameIsAlreadyExistException extends RuntimeException {
    public UserWithThatUsernameIsAlreadyExistException(final String message) {
        super(message);
    }
}
