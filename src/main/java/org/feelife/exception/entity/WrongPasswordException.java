package org.feelife.exception.entity;

public class WrongPasswordException extends RuntimeException {
    public WrongPasswordException(final String message) {
        super(message);
    }
}
