package org.feelife.exception.entity;

public class UserWithThatEmailDoesNotExistException extends RuntimeException {
    public UserWithThatEmailDoesNotExistException(final String message) {
        super(message);
    }
}
