package org.feelife.exception.entity;

public class UserWithThatEmailIsAlreadyExistException extends RuntimeException {
    public UserWithThatEmailIsAlreadyExistException(final String message) {
        super(message);
    }
}
