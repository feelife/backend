package org.feelife.exception.entity;

public class TokenNotFoundException extends RuntimeException {
    public TokenNotFoundException(final String message) {
        super(message);
    }
}
