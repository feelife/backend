package org.feelife.exception;


import org.feelife.exception.entity.*;
import org.feelife.exception.response.ResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Objects;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserWithThatEmailIsAlreadyExistException.class)
    public ResponseEntity<ResponseException> userIsAlreadyExist(final UserWithThatEmailIsAlreadyExistException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "foundEmail"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserWithThatUsernameIsAlreadyExistException.class)
    public ResponseEntity<ResponseException> userIsAlreadyExist(final UserWithThatUsernameIsAlreadyExistException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "foundUsername"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserWithThatEmailDoesNotExistException.class)
    public ResponseEntity<ResponseException> userNotFound(final UserWithThatEmailDoesNotExistException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "email"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(WrongPasswordException.class)
    public ResponseEntity<ResponseException> wrongPassword(final WrongPasswordException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "password"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TokenNotFoundException.class)
    public ResponseEntity<ResponseException> wrongToken(final TokenNotFoundException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "token"), HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseException> notValid(final MethodArgumentNotValidException exception) {
        return new ResponseEntity<>(new ResponseException(Objects.requireNonNull(exception.getFieldError()).getDefaultMessage(), "invalidField"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ActivityNotFoundException.class)
    public ResponseEntity<ResponseException> activityNotFound(final ActivityNotFoundException exception) {
        return new ResponseEntity<>(new ResponseException(exception.getMessage(), "activity"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseException> otherExceptions(final Exception exception) {
        return new ResponseEntity<>(new ResponseException("Internal server error", "server error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
