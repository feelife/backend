package org.feelife.util;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.Key;
import java.util.Date;

public class SecurityUtil {

    private SecurityUtil() {}

    public static final long SEC_IN_ONE_MONTH = 2678400L;

    private static final long MILLISEC_IN_ONE_MONTH = 2678400000L;

    private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static String getNewJwtToken(final String subject) {
        return Jwts.builder()
                .setSubject(subject)
                .signWith(key)
                .setExpiration(new Date(new Date().getTime() + MILLISEC_IN_ONE_MONTH))
                .compact();
    }

    public static boolean isJwtTokenInvalid(final String token) {
        if (token == null) {
            return true;
        }
        if (token.indexOf(' ') == -1) {
            return true;
        }
        String bearerValid = token.substring(0, token.indexOf(' '));
        return !bearerValid.equals("Bearer");
    }

    public static void isTokenExpired(final String token) throws JwtException {
        Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
    }

    public static String encodePassword(final String userPassword) {
        return passwordEncoder.encode(userPassword);
    }

    public static boolean validateResponsePassword(final String rawPassword, final String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
