package org.feelife.filter;

import org.feelife.util.SecurityUtil;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthorizationFilter implements Filter {

    private static final String AUTH_HEADER = "Authorization";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        final String requestToken = httpServletRequest.getHeader(AUTH_HEADER);
        if (!httpServletRequest.getMethod().equals(HttpMethod.OPTIONS.toString())) {
            if (SecurityUtil.isJwtTokenInvalid(requestToken)) {
                httpServletResponse.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid token filter");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
