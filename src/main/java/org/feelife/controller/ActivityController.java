package org.feelife.controller;

import org.feelife.model.dto.ActivityIdDto;
import org.feelife.model.entity.ActivityEntity;
import org.feelife.model.response.UsersActivityResponse;
import org.feelife.repository.AuthorizationRepository;
import org.feelife.repository.activity.ActivityRepository;
import org.feelife.repository.activity.UsersActivityRepository;
import org.feelife.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService, AuthorizationRepository repository, UsersActivityRepository activityRepository, ActivityRepository aRepo) {
        this.activityService = activityService;
    }

    @GetMapping("/new")
    public ResponseEntity<UsersActivityResponse> getNewActivity(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(activityService.getActivity(token));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ActivityEntity> getActivityById(@RequestHeader("Authorization") String token,
                                                          @PathVariable(name = "id") Integer activityId) {
        return ResponseEntity.ok(activityService.getActivityById(token, activityId));
    }

    @GetMapping("")
    public ResponseEntity<List<UsersActivityResponse>> getAllActivities(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(activityService.getAll(token));
    }

    @PostMapping(value = "/take")
    public ResponseEntity<UsersActivityResponse> takeActivity(@RequestHeader("Authorization") String token,
                                             @Valid @RequestBody ActivityIdDto activityIdDTO) {
        return ResponseEntity.ok(activityService.takeActivity(token, activityIdDTO));
    }

    @PostMapping(value = "/complete")
    public ResponseEntity<UsersActivityResponse> completeActivity(@RequestHeader("Authorization") String token,
                                             @Valid @RequestBody ActivityIdDto activityIdDTO) {
        return ResponseEntity.ok(activityService.completeActivity(token, activityIdDTO));
    }
}
