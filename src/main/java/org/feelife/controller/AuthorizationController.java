package org.feelife.controller;

import org.feelife.model.dto.UserLoginDto;
import org.feelife.model.dto.UserRegisterDTO;
import org.feelife.model.response.UserResponseToken;
import org.feelife.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/auth")
public class AuthorizationController {

    private final AuthorizationService authorizationService;

    @Autowired
    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseToken> addUser(@RequestBody @Valid UserRegisterDTO userRegisterDTO) {
        return ResponseEntity.ok(authorizationService.addUser(userRegisterDTO));
    }

    @PostMapping("/login")
    public ResponseEntity<UserResponseToken> loginUser(@RequestBody @Valid UserLoginDto userLoginDTO) {
        return ResponseEntity.ok(authorizationService.loginUser(userLoginDTO));
    }

    @GetMapping("/logout")
    public ResponseEntity<Void> logoutUser(@RequestHeader("Authorization") String token) {
        authorizationService.deleteUserToken(token);
        return ResponseEntity.ok().build();
    }
}
