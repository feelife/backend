package org.feelife.controller;

import org.feelife.service.TokenCheckerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/token")
public class TokenCheckerController {

    private final TokenCheckerService service;

    @Autowired
    public TokenCheckerController(final TokenCheckerService service) {
        this.service = service;
    }

    @GetMapping("/check")
    public ResponseEntity<Boolean> checkToken(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(service.isTokenValid(token));
    }
}
