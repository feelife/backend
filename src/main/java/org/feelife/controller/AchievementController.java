package org.feelife.controller;

import org.feelife.model.response.UserAchievementsResponse;
import org.feelife.service.AchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/achievement")
public class AchievementController {

    private final AchievementService service;

    @Autowired
    public AchievementController(final AchievementService service) {
        this.service = service;
    }

    @GetMapping("/my")
    public ResponseEntity<List<UserAchievementsResponse>> getUsersAchievement(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(service.getUsersAchievement(token));
    }
}
