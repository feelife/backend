package org.feelife.repository;

import org.feelife.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorizationRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findUserByUsername(String username);

    @Query("SELECT u FROM UserEntity u WHERE u.email = ?1")
    Optional<UserEntity> findUserByEmail(String email);

    @Query("SELECT u.id FROM UserEntity u WHERE u.email = ?1")
    Optional<Integer> findIdByEmail(String email);
}
