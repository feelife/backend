package org.feelife.repository;

import org.feelife.model.entity.BearerTokenEntity;
import org.feelife.model.response.UserWithBearerTokenIdResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface BearerTokenRepository extends JpaRepository<BearerTokenEntity, Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM BearerTokenEntity bt WHERE bt.userId = ?1")
    void deleteBearerTokenEntityByUserId(Integer userId);

    @Query(value = "SELECT bt.userId FROM BearerTokenEntity bt WHERE bt.token = ?1")
    Optional<Integer> getUserIdByToken(String token);


    @Query(value = "SELECT bt.token FROM BearerTokenEntity bt WHERE bt.userId = ?1")
    Optional<Integer> getTokenByUserId(Integer id);

    @Query(value =
            "SELECT new org.feelife.model.response.UserWithBearerTokenIdResponse(u.id, bt.id, u.password, u.username, u.email)" +
                    "FROM BearerTokenEntity bt " +
            "JOIN UserEntity u ON bt.userId = u.id WHERE u.email = ?1")
    Optional<UserWithBearerTokenIdResponse> getIdByUserEmail(String email);
}
