package org.feelife.repository.achievement;

import org.feelife.model.entity.UserAchievementEntity;
import org.feelife.model.entity.keys.UserAchievementIds;
import org.feelife.model.response.UserAchievementsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface UserAchievementRepository extends JpaRepository<UserAchievementEntity, UserAchievementIds> {

    @Query("SELECT new org.feelife.model.response.UserAchievementsResponse(ae.name)\n" +
            "FROM UserAchievementEntity uae\n" +
            "LEFT JOIN AchievementEntity ae on uae.achievementId = ae.id\n" +
            "WHERE uae.userId = ?1")
    Optional<List<UserAchievementsResponse>> getUserAchievements(Integer userId);
}
