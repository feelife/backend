package org.feelife.repository.achievement;

import org.feelife.model.entity.AchievementEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AchievementRepository extends JpaRepository<AchievementEntity, Integer> {
}
