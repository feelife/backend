package org.feelife.repository.achievement;

import org.feelife.model.entity.AchievementActivityEntity;
import org.feelife.model.entity.keys.AchievementActivityIds;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AchievementActivityRepository extends JpaRepository<AchievementActivityEntity, AchievementActivityIds> {
}
