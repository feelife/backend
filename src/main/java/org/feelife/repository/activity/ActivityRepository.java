package org.feelife.repository.activity;

import org.feelife.model.entity.ActivityEntity;
import org.feelife.model.response.UsersActivityResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Repository
public interface ActivityRepository extends JpaRepository<ActivityEntity, Integer> {

    @Query(value = "SELECT ae.id FROM ActivityEntity ae")
    Optional<HashSet<Integer>> getAllActivitiesID();

    @Query(value =
            "SELECT activity.id\n" +
            "FROM ActivityEntity activity\n" +
            "WHERE activity.id not in (\n" +
            "    SELECT users_activity.activityId\n" +
            "    FROM UsersActivityEntity users_activity\n" +
            "    WHERE (users_activity.isTaken = true\n" +
            "       OR users_activity.isCompleted = true) AND users_activity.userId = ?1)")
    Optional<List<Integer>> getAllAcceptedActivities(Integer userId);

    @Query(value =
            "SELECT a\n" +
            "FROM UsersActivityEntity users_activity\n" +
            "LEFT JOIN ActivityEntity a on users_activity.activityId = a.id\n" +
            "WHERE users_activity.activityId = ?1 AND users_activity.userId = ?2")
    Optional<ActivityEntity> getUsersActivityByActivityID(Integer activityId, Integer userId);

    @Query(value =
            "SELECT new org.feelife.model.response.UsersActivityResponse(" +
                    "a.id, a.name, a.description, a.estimatedTime, users_activity.isTaken, users_activity.isCompleted)\n" +
            "FROM UsersActivityEntity users_activity\n" +
            "         LEFT JOIN ActivityEntity a on users_activity.activityId = a.id " +
            "WHERE users_activity.userId = ?1 AND users_activity.isTaken = true AND users_activity.isCompleted = false ORDER BY a.name")
    Optional<List<UsersActivityResponse>> getAllUsersActivities(Integer userId);
}
