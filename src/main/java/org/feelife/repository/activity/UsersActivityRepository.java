package org.feelife.repository.activity;

import org.feelife.model.entity.UsersActivityEntity;
import org.feelife.model.entity.keys.UsersActivityIds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Optional;

@Repository
public interface UsersActivityRepository extends JpaRepository<UsersActivityEntity, UsersActivityIds> {

    @Query(value = "SELECT ua.activityId FROM UsersActivityEntity ua WHERE ua.userId = ?1")
    Optional<HashSet<Integer>> findAllActivityByUserId(Integer userId);

    @Query(value = "SELECT ua.activityId FROM UsersActivityEntity ua WHERE ua.userId = ?1 AND ua.isCompleted = true")
    Optional<HashSet<Integer>> findAllCompletedActivitiesByUserId(Integer userId);
}
