package org.feelife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeelifeApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeelifeApplication.class, args);
    }
}
