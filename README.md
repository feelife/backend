# Info

The repository contains backend part of [feelife](https://gitlab.com/feelife/feelife) project.

## How to run
### Setting up database using docker
1. Install `docker` and `docker-compose`

2. Set environment variables `POSTGRES_USER`, `POSTGRES_DATABASE_NAME`, `POSTGRES_PASSWORD`.

3. Run database: `docker-compose up`

### Running backend on your host
1. Install `JDK 11` 

2. Compile single .jar file with dependencies: `mvn clean package spring-boot:repackage -DskipTests`

3. Set environment variables listed in `.env.example`

4. `java -jar target/*.jar`

### Running backend inside of docker container
1. `docker build -t feelife/backend:latest .`

2. `docker run -p <LISTENING_PORT_ON_HOST>:<SERVER_PORT_ENV_VALUE> --env-file=<PATH_TO_YOUR_ENV_FILE> feelife/backend:latest`

NOTE: if you will run backend and database inside of docker,
make sure to connect both containers to one docker network and
set hostname for database for easier working with `POSTGRES_HOST`
variable, because changing again and again IP of database from dynamically
selected address in docker network is not the work, what we 
want to do.

## Troubleshooting
1. If some commands cannot be found, make sure that `PATH` 
environment variable points to directories with 
installed programs.
