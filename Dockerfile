FROM openjdk:11-jdk
WORKDIR /app
COPY ./target/ /app/target/
CMD java -jar /app/target/*.jar
